import cv2
import time
roi = []
def get_roi(even, x, y, flag, param):
    if even == cv2.EVENT_LBUTTONUP:
        image[y-4:y+4, x-4:x+4, :] = (0, 0, 255)
        roi.append([x, y])
    if len(roi)==2:
        cv2.rectangle(image, (roi[0][0], roi[0][1]), (roi[1][0], roi[1][1]), (0, 0, 255))
        cv2.putText(image, 'Extracting video with ROI',
                    (roi[0][0], roi[1][1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        cv2.imshow("ROI", image)
        cv2.waitKey()
        extractImages(pathIn, pathOut, roi)
        cv2.destroyAllWindows()

def extractImages(pathIn, pathOut, roi):
    count = 0
    vidcap = cv2.VideoCapture(pathIn)
    fps_video = vidcap.get(5)
    fps = 10 #fps_video
    if fps>fps_video: fps=fps_video
    frame_total_video = vidcap.get(cv2.CAP_PROP_FRAME_COUNT)
    time = 1000 / fps

    print('Loading...')
    print(fps_video)
    print(frame_total_video)

    success = True
    while success and count<frame_total_video/(fps_video/fps)-1:
        if vidcap.isOpened():
            vidcap.set(cv2.CAP_PROP_POS_MSEC, (count*time))    # added this line
            success, image = vidcap.read()
            image_write = image[roi[0][1]:roi[1][1], roi[0][0]:roi[1][0], :]
            cv2.imwrite(pathOut + "frame%d.jpg" % count, image_write)     # save frame as JPEG file
            count = count + 1
        else:
            break
    print('Complete: ', count)

pathIn = '/home/cuong/Desktop/BT_kythuatnhandang/video1.avi'
pathOut = '/home/cuong/Desktop/BT_kythuatnhandang/khoai/'



vidcap = cv2.VideoCapture(pathIn)
success, image = vidcap.read()
cv2.namedWindow('ROI')
cv2.setMouseCallback('ROI', get_roi)
while(1):
    cv2.imshow('ROI', image)
    if cv2.waitKey(20) & 0xFF == 27:
        break

