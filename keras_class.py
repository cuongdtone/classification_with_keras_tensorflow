import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from glob import glob
import cv2
import numpy as np
import tensorflow as tf
tf.get_logger().setLevel('INFO')

def predict_classification(img, model):
    #img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    data =[]
    image = cv2.resize(img, (224, 224))
    data.append(image)
    data = np.array(data, dtype="float") / 127.0 - 1
    predict = model.predict(data)
    print(predict)
    return predict[0][0]

model = tf.keras.models.load_model('/home/cuong/Downloads/converted_keras/keras_model.h5')
file_label = open('/home/cuong/Downloads/converted_keras/labels.txt', 'r')
class_name = file_label.readlines()
file_label.close()
for i, name in enumerate(class_name):
    name = name.split(' ')
    name.pop(0)
    name = ' '.join(name)
    class_name[i] = name.rstrip('\n')
print(class_name)
img_path = '/home/cuong/Desktop/BT_kythuatnhandang/class2'
list_img = glob(img_path + '/*.jpg')
list_img.sort()
for i in list_img:
    image = cv2.imread(i)
    pred = predict_classification(image, model)
    print(round(pred))
