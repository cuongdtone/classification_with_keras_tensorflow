import cv2

input_path = '/home/cuong/Desktop/VideoThe_camera360/VID_20210826_141450.mp4'
video_filename = '/home/cuong/Desktop/video/video5.avi'  # record detect (no gpu)
# Create a video capture object, in this case we are reading the video from a file
vid_capture = cv2.VideoCapture(input_path)

if (vid_capture.isOpened() == False):
    print("Error opening the video file")

else:

    fps = vid_capture.get(5)
    print('Frames per second : ', fps, 'FPS')

    frame_count = vid_capture.get(7)
    print('Frame count : ', frame_count)
    width = int(vid_capture.get(3))
    height = int(vid_capture.get(4))
    fps = vid_capture.get(cv2.CAP_PROP_FPS)
    fps_video = fps if fps <= 120 else 30

    # print(fps_video)


    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    video_write = cv2.VideoWriter(video_filename, fourcc, fps_video, (width, height))

global frame_index
global time_list
time_list = []

def get_time(even, x, y, flags, param):
    if even == cv2.EVENT_LBUTTONUP:
        time_list.append(frame_index)
        if len(time_list) % 2 == 0:
            print('end at ', frame_index)
        else:
            print('start at ', frame_index)



frame_index = 1
while (vid_capture.isOpened()):
    cv2.namedWindow('Frame')
    cv2.setMouseCallback('Frame', get_time)

    ret, frame = vid_capture.read()
    frame_index += 1
    if ret == True:
        cv2.imshow('Frame', cv2.resize(frame, (1280, 720)))
        key = cv2.waitKey(20)
        if key == ord('q'):
            cv2.destroyAllWindows()
            break
        # 20 is in milliseconds, try to increase the value, say 50 and observe

    else:
        break

print(time_list)
'''
time_list = [ 3348, 3454]
vid_cap = cv2.VideoCapture(input_path)
frame_index = 1

while (vid_cap.isOpened()):
    ret, frame = vid_cap.read()
    frame_index += 1
    print(ret)
    if frame_index>time_list[1]:
        break
    if ret == True:
        if frame_index >= time_list[0] and frame_index<=time_list[1]:
            video_write.write(frame)
    else:
        break
'''

vid_capture.release()
cv2.destroyAllWindows()