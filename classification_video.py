import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import cv2
import numpy as np
import tensorflow as tf
tf.get_logger().setLevel('INFO')

rtsp = 'http://admin:123@192.168.61.181:8080/video'
model_path = '/home/cuong/Downloads/converted_keras/keras_model.h5'

def predict_classification(img, model):
    #img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    data =[]
    image = cv2.resize(img, (224, 224))
    data.append(image)
    data = np.array(data, dtype="float") / 127.0 - 1
    predict = model.predict(data)
    return predict
roi = []
def mouse_event(even, x, y, flag, param):
    if even == cv2.EVENT_LBUTTONUP:
        image[y-4:y+4, x-4:x+4, :] = (0, 0, 255)
        roi.append([x, y])
    if len(roi)==2:
        #cv2.rectangle(image, (roi[0][0], roi[0][1]), (roi[1][0], roi[1][1]), (0, 0, 255))
        #cv2.putText(image, 'Extracting video with ROI',
        #            (roi[0][0], roi[1][1]), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        #cv2.imshow("Capture", image)
        #cv2.waitKey()
        classification(rtsp, roi)
        cv2.destroyAllWindows()
def classification(rtsp, roi):

    vid_cap = cv2.VideoCapture(rtsp)
    while True:
        ret, image = vid_cap.read()
        image = cv2.resize(image, (1280, 720))
        image_object = image[roi[0][1]:roi[1][1], roi[0][0]:roi[1][0], :]
        pred = predict_classification(image_object, model)
        cv2.rectangle(image, (roi[0][0], roi[0][1]), (roi[1][0], roi[1][1]), (0, 0, 255))
        if pred[0][0]<0.5:
            cl = 1
        else:
            cl = 0
        cv2.putText(image, 'Day la ' + class_name[cl],
                   (roi[0][0], roi[0][1]-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        cv2.putText(image, class_name[0] + ': ' + str(round(pred[0][0]*100)) + '%',
                   (roi[0][0], roi[1][1]+50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        cv2.putText(image, class_name[1] + ': ' + str(round(pred[0][1]*100)) + '%',
                   (roi[0][0], roi[1][1]+100), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        cv2.imshow('Capture', image)

        key = cv2.waitKey(5)
        if (key & 0xFF00 == 0):  # normal keys press
            if (key & 0xFF) in [ord('q'), ord('Q'), 27]:
                break


def predict_classification(img, model):
    #img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    data =[]
    image = cv2.resize(img, (224, 224))
    data.append(image)
    data = np.array(data, dtype="float") / 127.0 - 1
    predict = model.predict(data)
    return predict

model = tf.keras.models.load_model(model_path)
label_path = model_path.split('/')
label_path.pop()
label_path = '/'.join(label_path) + '/labels.txt'
file_label = open(label_path, 'r')
class_name = file_label.readlines()
file_label.close()
for i, name in enumerate(class_name):
    name = name.split(' ')
    name.pop(0)
    name = ' '.join(name)
    class_name[i] = name.rstrip('\n')
print(class_name)


video_capture = cv2.VideoCapture(rtsp)

ret, image = video_capture.read()
image = cv2.resize(image, (1280, 720))
cv2.namedWindow('Capture')
cv2.setMouseCallback('Capture', mouse_event)
while (1):
    cv2.imshow('Capture', image)
    if cv2.waitKey(20) & 0xFF == 27:
        break